package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

const (
	walletIdToGetTokens   = "7a854d7d28c971977d678aedee5e4ffa4f968495"
	walletAddrToPutTokens = "addr_test1qr866c0jguduvq8d280jwuazd8wa8fkk688euuqetu90m46m48n0dver3km5fey8g5qupchwjnv06getmyxwhrvl6hdq4erur6"
)

type TRequestBody struct {
	Passphrase string    `json:"passphrase"`
	Payments   TPayments `json:"payments"`
}

type TPayments []TPayment

type TPayment struct {
	Address string   `json:"address"`
	Ammount TAmmount `json:"amount"`
}

type TAmmount struct {
	Quantity uint64 `json:"quantity"`
	Unit     string `json:"unit"`
}

func main() {

	var requestBody TRequestBody = TRequestBody{Passphrase: "test123456", Payments: TPayments{}}

	var singlePayment TPayment = TPayment{
		Address: walletAddrToPutTokens,
		Ammount: TAmmount{}}

	var singleAmmount TAmmount = TAmmount{Quantity: 250000000, Unit: "lovelace"}

	singlePayment.Ammount = singleAmmount

	var tempPayments TPayments = TPayments{}

	tempPayments = append(tempPayments, singlePayment)

	requestBody.Payments = tempPayments

	json_data, err := json.Marshal(requestBody)

	if err != nil {
		log.Fatal(err)
	}

	resp, err := http.Post("http://localhost:1337/v2/wallets/"+walletIdToGetTokens+"/transactions",
		"application/json",
		bytes.NewBuffer(json_data))

	if err != nil {
		log.Fatal(err)
	}

	var res map[string]interface{}

	json.NewDecoder(resp.Body).Decode(&res)

	fmt.Println(res["json"])
}
